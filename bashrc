#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases


alias ls='ls --color=auto'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -h'                      # show sizes in MB
alias more=less
alias clima="curl http://wttr.in"
alias grep='grep --color'
alias limpiar='sudo pacman -Rns $(pacman -Qdtq) ; sudo paccache -rk1 ; sudo paccache -ruk0'
alias updmirr='sudo reflector --verbose --latest 200 --sort rate --save /etc/pacman.d/mirrorlist'

shopt -s histappend

buscarsubs () {
if [[ -f $1 ]] ; then
	subdl --lang=spa --interactive "$1"
else
	echo "'$1' no es un archivo valido"
fi
}


bajasubs () {
for i in * 
do
	if test -f "$i"
		then
		echo "Buscando subs para $i..."
		subdl --lang=spa --download=most-downloaded --existing=query "$i"
	fi
done
}


PS1='[\u@\h \W]\$ '

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion
export BROWSER=/usr/bin/qutebrowser
export EDITOR=/usr/bin/vim
export VISUAL=/usr/bin/vim
export LC_ALL=es_AR.UTF-8
export LANG=es_AR.UTF-8
export LANGUAGE=es_AR.UTF-8
export TERM=termite
export HISTSIZE=3000
export HISTFILESIZE=3000
alias convsubs="sed -i 's/á/a/g; s/é/e/g; s/í/i/g; s/ó/o/g; s/ú/u/g; s/Á/A/g; s/É/E/g; s/Í/I/g; s/Ó/O/g; s/Ú/U/g; s/Ñ/N/g; s/ñ/n/g; s/¿//g; s/¡//g'"
